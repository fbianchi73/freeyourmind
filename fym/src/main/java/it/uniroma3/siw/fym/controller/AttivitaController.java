package it.uniroma3.siw.fym.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import it.uniroma3.siw.fym.model.Attivita;
import it.uniroma3.siw.fym.model.Socio;
import it.uniroma3.siw.fym.service.AttivitaService;
import it.uniroma3.siw.fym.service.FileService;
import it.uniroma3.siw.fym.service.PrenotazioneService;
import it.uniroma3.siw.fym.service.SocioService;
import it.uniroma3.siw.fym.validator.AttivitaValidator;

@Controller
public class AttivitaController {

	@Autowired
	private AttivitaService attivitaService;

	@Autowired
	private FileService fileService;
	
	@Autowired
	private PrenotazioneService prenotazioneService;
	
	@Autowired
	private SocioService socioService;

	@Autowired
	private AttivitaValidator attivitaValidator;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value="/attivita", method = RequestMethod.GET)
	public String getAllNonOrganizzate(Model model) {
		logger.debug("getAllAttivitaNonOrganizzate");
		if(!SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ANONYMOUS"))) {
			String email;
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth.getPrincipal().getClass().equals(User.class))
			{
				UserDetails user = (UserDetails) auth.getPrincipal();
				email = user.getUsername();
			}
			else {
				OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
		        email = oAuth2User.getAttribute("email");
			}
			model.addAttribute("attivita", attivitaService.getAllNonOrganizzateByEmail(email));
			model.addAttribute("isOrganizzatore", false);
			model.addAttribute("socio", socioService.getByEmail(email));
		}
		else 
			model.addAttribute("attivita", attivitaService.getAll());
		return "attivita.html";
	}

	@RequestMapping(value="/attivitaOrganizzate", method = RequestMethod.GET)
	public String getAllOrganizzate(Model model) {
		logger.debug("getAllAttivitaOrganizzate");
		String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
		model.addAttribute("attivita", attivitaService.getAllOrganizzateByEmail(email));
		model.addAttribute("isOrganizzatore", true);
		return "attivita.html";
	}

	@RequestMapping(value="/dettaglioAttivita/{id}", method = RequestMethod.GET)
	public String getAttivitaOrganizzata(Model model, @PathVariable("id") Long id) {
		logger.debug("getDettaglioAttivita");
		String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
		Socio s = socioService.getByEmail(email);
		Attivita a = attivitaService.getById(id);
		if(a.getOrganizzatore().equals(s))
		{
			model.addAttribute("attivita", attivitaService.getById(id));
			return "dettaglioAttivita.html";
		}
		else {
			model.addAttribute("attivita", attivitaService.getAllOrganizzateByEmail(s.getEmail()));
			model.addAttribute("isOrganizzatore", true);
			return "attivita.html";
		}
	}

	@RequestMapping(value="/addAttivita", method=RequestMethod.GET)
	public String addAttivita(Model model) {
		logger.debug("addAttivita");
		model.addAttribute("attivita", new Attivita());
		//model.addAttribute("indirizzo", new Indirizzo());
		return "attivitaForm.html";
	}

	@RequestMapping(value="/confermaAttivita", method=RequestMethod.POST)
	public String confermaAttivita(@ModelAttribute("attivita") Attivita attivita, 
			Model model, BindingResult bindingResult, @RequestParam("file") MultipartFile file) {
		this.attivitaValidator.validate(attivita, bindingResult);
		logger.debug("confermaAttivita");
		if (!bindingResult.hasErrors()) {
			attivita.setImmagine(fileService.upload(file));
			model.addAttribute("attivita", attivita);
			return "confermaAttivita.html";
		}
		return "attivitaForm.html";
	}

	@RequestMapping(value = "/attivita", method = RequestMethod.POST)
	public String newAttivita(@ModelAttribute("attivita") Attivita attivita, 
			Model model) {
		logger.debug("salvaAttivita");
		this.attivitaService.save(attivita);
		String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
		Socio s = socioService.getByEmail(email);
		model.addAttribute("attivita", attivitaService.getAllOrganizzateByEmail(s.getEmail()));
		model.addAttribute("isOrganizzatore", true);
		return "attivita.html";
	}

	@RequestMapping(value="/modificaAttivita/{id}", method=RequestMethod.GET)
	public String editAttivita(Model model, @PathVariable("id") Long id) {
		logger.debug("editAttivita");
		String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
		Socio s = socioService.getByEmail(email);
		Attivita a = attivitaService.getById(id);
		if(a.getOrganizzatore().equals(s))
		{
			model.addAttribute("attivita", attivitaService.getById(id));
			return "attivitaForm.html";
		}
		else {
			model.addAttribute("attivita", attivitaService.getAllOrganizzateByEmail(s.getEmail()));
			model.addAttribute("isOrganizzatore", true);
			return "attivita.html";
		}
	}

	@RequestMapping(value="/eliminaAttivita", method=RequestMethod.POST)
	public String deleteAttivita(Model model, @ModelAttribute("attivita") Attivita attivita) {
		logger.debug("deleteAttivita");
		attivitaService.deleteById(attivita.getId());
		String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
		model.addAttribute("prossimaOrganizzata", attivitaService.getProssimaAttivitaOrganizzata(email));
		model.addAttribute("prossimaPrenotata", prenotazioneService.getProssimaAttivitaPrenotata(email));
		return "home.html";
	}
}

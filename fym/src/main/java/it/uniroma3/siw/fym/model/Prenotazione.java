package it.uniroma3.siw.fym.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Prenotazione {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String note;
	
	@ManyToOne(fetch=FetchType.LAZY)
	private Socio socio;
	
	@ManyToOne(fetch=FetchType.LAZY)
	private Attivita attivita;
	
	public Prenotazione() {}
	
	public Prenotazione(Attivita attivita, Socio socio, String note) {
		this.attivita = attivita;
		this.socio = socio;
		this.note = note;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

	public Attivita getAttivita() {
		return attivita;
	}

	public void setAttivita(Attivita attivita) {
		this.attivita = attivita;
	}
		
}

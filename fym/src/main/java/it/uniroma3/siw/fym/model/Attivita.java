package it.uniroma3.siw.fym.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Attivita {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;

	@Column(nullable=false)
	private String descrizione;

	private String immagine;
	
	@Column(nullable=false)
	private Integer postiDisponibili;
	
	@Column(nullable=false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date data;

	@ManyToOne(fetch=FetchType.LAZY)
	private Socio organizzatore;
	
	@OneToMany(mappedBy="attivita", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Prenotazione> prenotazioni;

	@OneToMany(mappedBy="attivita", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Recensione> recensioni;
	
	public Attivita() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public Integer getPostiDisponibili() {
		return postiDisponibili;
	}

	public void setPostiDisponibili(Integer postiDisponibili) {
		this.postiDisponibili = postiDisponibili;
	}

	@DateTimeFormat(pattern="yyyy-MM-dd")
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Socio getOrganizzatore() {
		return organizzatore;
	}

	public void setOrganizzatore(Socio organizzatore) {
		this.organizzatore = organizzatore;
	}

	public List<Prenotazione> getPrenotazioni() {
		return prenotazioni;
	}

	public void setPrenotazioni(List<Prenotazione> prenotazioni) {
		this.prenotazioni = prenotazioni;
	}

	public List<Recensione> getRecensioni() {
		return recensioni;
	}

	public void setRecensioni(List<Recensione> recensioni) {
		this.recensioni = recensioni;
	}
	
	public String getImmagine() {
		return immagine;
	}

	public void setImmagine(String immagine) {
		this.immagine = immagine;
	}
	
	public int getRating() {
		if(recensioni.size() == 0)	return 0;
		int rating = 0;
		for(Recensione r : recensioni)
			rating += r.getPunteggio();
		return rating/recensioni.size();
	}
	
}

package it.uniroma3.siw.fym.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Recensione {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private Integer punteggio;
	
	@Column(nullable=false)
	private String descrizione;
	
	@ManyToOne(fetch=FetchType.LAZY)
	private Socio socio;
	
	@ManyToOne(fetch=FetchType.LAZY)
	private Attivita attivita;
	
	public Recensione() {}

	public Recensione(Integer punteggio, String descrizione, Socio socio, Attivita attivita) {
		this.punteggio = punteggio;
		this.descrizione = descrizione;
		this.socio = socio;
		this.attivita = attivita;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPunteggio() {
		return punteggio;
	}

	public void setPunteggio(Integer punteggio) {
		this.punteggio = punteggio;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

	public Attivita getAttivita() {
		return attivita;
	}

	public void setAttivita(Attivita attivita) {
		this.attivita = attivita;
	}	
}
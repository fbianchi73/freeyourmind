package it.uniroma3.siw.fym.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.fym.model.Credenziali;
import it.uniroma3.siw.fym.repository.CredenzialiRepository;

@Service
public class CredenzialiService {
	@Autowired
	protected CredenzialiRepository credenzialiRepository;
	
	@Autowired
	protected PasswordEncoder passwordEncoder;
	
	@Transactional
	public Credenziali getCredenziali(Long id) {
		Optional<Credenziali> result = this.credenzialiRepository.findById(id);
		return result.orElse(null);
	}
	
	@Transactional
	public Credenziali getCredenziali(String username) {
		Optional<Credenziali> result = this.credenzialiRepository.findByUsername(username);
		return result.orElse(null);
	}
	
	@Transactional
	public Credenziali saveCredenziali(Credenziali credenziali, String role) {
		credenziali.setRole(role);
		return this.credenzialiRepository.save(credenziali);
	}
	
	public Credenziali saveCredenziali(Credenziali credenziali) {
		credenziali.setPassword(passwordEncoder.encode(credenziali.getPassword()));
		return this.saveCredenziali(credenziali, Credenziali.DEFAULT_ROLE);
	}
	
	@Transactional
	public List<Credenziali> findAllNotAdmin(){
		return (List<Credenziali>) this.credenzialiRepository.findAllByRoleIs("DEFAULT");
	}
	
}

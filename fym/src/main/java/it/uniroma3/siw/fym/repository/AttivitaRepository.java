package it.uniroma3.siw.fym.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.fym.model.Attivita;

public interface AttivitaRepository extends CrudRepository<Attivita, Long> {
	public List<Attivita> findAllByOrderByDataDesc();
	public List<Attivita> findAllByOrganizzatoreEmailOrderByDataDesc(String organizzatoreEmail);
	public List<Attivita> findAllByOrganizzatoreEmailIsNotOrderByDataDesc(String organizzatoreEmail);
	public Attivita findTopByOrganizzatoreEmailAndDataAfterOrderByDataAsc(String organizzatoreEmail, Date date);
}

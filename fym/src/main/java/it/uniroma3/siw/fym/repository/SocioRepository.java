package it.uniroma3.siw.fym.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.fym.model.Socio;

public interface SocioRepository extends CrudRepository<Socio, Long> {

	Socio findByEmail(String email);
}

package it.uniroma3.siw.fym.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.fym.model.Credenziali;

public interface CredenzialiRepository extends CrudRepository<Credenziali, Long>{
	public Optional<Credenziali> findByUsername(String username);
	public List<Credenziali> findAllByRoleIs(String role);
}

package it.uniroma3.siw.fym.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.fym.model.Provider;
import it.uniroma3.siw.fym.model.Socio;
import it.uniroma3.siw.fym.repository.SocioRepository;

@Service
public class SocioService {
	@Autowired
	private SocioRepository socioRepository;

	public List<Socio> getAll(){
		return (List<Socio>) socioRepository.findAll();
	}
	
	public Socio getById(Long id) {
		return socioRepository.findById(id).get();
	}
	
	public Socio getByEmail(String email) {
		return socioRepository.findByEmail(email);
	}

	public boolean alreadyExists(Socio socio) {
		if(socioRepository.findById(socio.getId()).isPresent())
			return true;
		return false;
	}
	
	public void saveOrUpdate(Socio socio) {
		socioRepository.save(socio);
	}

	public void processOAuthPostLogin(String email, String nome, String cognome) {
        Socio socio = socioRepository.findByEmail(email);
        if (socio == null) {
            Socio nuovoSocio = new Socio();
            nuovoSocio.setEmail(email);
            nuovoSocio.setNome(nome);
            nuovoSocio.setCognome(cognome);
            nuovoSocio.setProvider(Provider.GOOGLE);
            socioRepository.save(nuovoSocio);        
        }
    }
}

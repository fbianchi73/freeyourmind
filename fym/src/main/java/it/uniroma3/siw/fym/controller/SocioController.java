package it.uniroma3.siw.fym.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.fym.model.Socio;
import it.uniroma3.siw.fym.service.SocioService;
import it.uniroma3.siw.fym.validator.SocioValidator;

@Controller
public class SocioController {
	@Autowired
	private SocioService socioService;
	
	@Autowired
	private SocioValidator socioValidator;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
	@RequestMapping(value = "/socio", method = RequestMethod.POST)
    public String newSocio(@ModelAttribute("socio") Socio socio, 
    									Model model, BindingResult bindingResult) {
		logger.debug("newOrEditSocio");
    	this.socioValidator.validate(socio, bindingResult);
        if (!bindingResult.hasErrors()) {
        	this.socioService.saveOrUpdate(socio);
        }
        return "profiloForm.html";
    }
	
	@RequestMapping(value = "/profilo", method = RequestMethod.GET)
    public String viewSocio(Model model) {
		logger.debug("profile");
		String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
        model.addAttribute("socio", socioService.getByEmail(email));
        return "profiloForm.html";
    }
	
}

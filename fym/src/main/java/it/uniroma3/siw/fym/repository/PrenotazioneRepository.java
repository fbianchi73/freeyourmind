package it.uniroma3.siw.fym.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.fym.model.Prenotazione;

@Repository
public interface PrenotazioneRepository extends CrudRepository<Prenotazione, Long>{
	public List<Prenotazione> findAllBySocioIdOrderByAttivitaDataDesc(Long id);	
	public Prenotazione findTopBySocioEmailAndAttivitaDataAfterOrderByAttivitaDataAsc(String organizzatoreEmail, Date date);
}

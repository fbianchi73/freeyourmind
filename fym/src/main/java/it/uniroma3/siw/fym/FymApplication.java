package it.uniroma3.siw.fym;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FymApplication {
	public static void main(String[] args) {
		SpringApplication.run(FymApplication.class, args);
	}
}

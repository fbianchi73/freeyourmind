package it.uniroma3.siw.fym.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.uniroma3.siw.fym.model.Recensione;
import it.uniroma3.siw.fym.model.Socio;
import it.uniroma3.siw.fym.service.AttivitaService;
import it.uniroma3.siw.fym.service.PrenotazioneService;
import it.uniroma3.siw.fym.service.RecensioneService;
import it.uniroma3.siw.fym.service.SocioService;
import it.uniroma3.siw.fym.validator.RecensioneValidator;

@Controller
public class RecensioneController {

	@Autowired
	private RecensioneService recensioneService;

	@Autowired
	private AttivitaService attivitaService;
	
	@Autowired
	private PrenotazioneService prenotazioneService;
	
	@Autowired
	private SocioService socioService;
	
	@Autowired
	private RecensioneValidator recensioneValidator;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value="/recensioni", method = RequestMethod.GET)
    public String getAll(Model model) {
    	logger.debug("getAllRecensioni");
    	String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
    	model.addAttribute("recensioni", recensioneService.getAllBySocioEmail(email));
        return "recensioni.html";
    }
	
	@RequestMapping(value="/eliminaRecensione", method = RequestMethod.POST)
    public String delete(Model model, @RequestParam("id") Long id) {
    	logger.debug("deleteRecensione");
    	recensioneService.deleteById(id);
    	String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
		model.addAttribute("recensioni", recensioneService.getAllBySocioEmail(email));
		return "recensioni.html";
    }
	
	@RequestMapping(value="/addRecensione/{id}", method = RequestMethod.GET)
    public String addRecensione(Model model, @PathVariable("id") Long id) {
    	logger.debug("addRecensione");
    	Recensione recensione = new Recensione();
    	recensione.setAttivita(attivitaService.getById(id));
    	model.addAttribute("recensione", recensione);
    	return "recensioneForm.html";
    }
	
	@RequestMapping(value = "/recensione", method = RequestMethod.POST)
    public String newRecensione(@ModelAttribute("recensione") Recensione recensione, 
    									Model model, BindingResult bindingResult) {
		this.recensioneValidator.validate(recensione, bindingResult);
		if (!bindingResult.hasErrors()) {
			String email;
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth.getPrincipal().getClass().equals(User.class))
			{
				UserDetails user = (UserDetails) auth.getPrincipal();
				email = user.getUsername();
			}
			else {
				OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
		        email = oAuth2User.getAttribute("email");
			}
			Socio s = socioService.getByEmail(email);
			recensione.setSocio(s);
			this.recensioneService.saveOrUpdate(recensione);
			model.addAttribute("prossimaOrganizzata", attivitaService.getProssimaAttivitaOrganizzata(email));
			model.addAttribute("prossimaPrenotata", prenotazioneService.getProssimaAttivitaPrenotata(email));
            return "home.html";
		}
		return "recensioneForm.html";
	}
}
package it.uniroma3.siw.fym.service;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.fym.model.Attivita;
import it.uniroma3.siw.fym.model.Socio;
import it.uniroma3.siw.fym.repository.AttivitaRepository;

@Service
public class AttivitaService {
	@Autowired
	private AttivitaRepository attivitaRepository;
	
	@Autowired
	private SocioService socioService;
		
	@Transactional
	public List<Attivita> getAll(){
		return (List<Attivita>) attivitaRepository.findAllByOrderByDataDesc();
	}

	@Transactional
	public List<Attivita> getAllOrganizzateByEmail(String email){
		return (List<Attivita>) attivitaRepository.findAllByOrganizzatoreEmailOrderByDataDesc(email);
	}
	
	@Transactional
	public List<Attivita> getAllNonOrganizzateByEmail(String email){
		return (List<Attivita>) attivitaRepository.findAllByOrganizzatoreEmailIsNotOrderByDataDesc(email);
	}
	
	@Transactional
	public Attivita getById(Long id){
		return attivitaRepository.findById(id).get();
	}
	
	@Transactional
	public void riduciPosti(Attivita a) {
		a.setPostiDisponibili(a.getPostiDisponibili()-1);
		attivitaRepository.save(a);
	}
	
	@Transactional
	public void save(Attivita attivita) {
		String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
		Socio s = socioService.getByEmail(email);
		attivita.setOrganizzatore(s);
		attivitaRepository.save(attivita);
	}

	@Transactional
	public void deleteById(Long id) {
		attivitaRepository.deleteById(id);
	}

	public void aggiungiPosti(Attivita a) {
		a.setPostiDisponibili(a.getPostiDisponibili()+1);
		attivitaRepository.save(a);
	}

	@Transactional
	public Attivita getProssimaAttivitaOrganizzata(String email) {
		return attivitaRepository.findTopByOrganizzatoreEmailAndDataAfterOrderByDataAsc(email, Date.from(Instant.now()));
	}
}

package it.uniroma3.siw.fym.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.fym.model.Socio;

@Component
public class RecensioneValidator implements Validator{	
    private static final Logger logger = LoggerFactory.getLogger(SocioValidator.class);

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "attivita.id", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "punteggio", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descrizione", "required");

		if (!errors.hasErrors()) {
			logger.debug("confermato: valori non nulli");
/*			if (this.socioService.alreadyExists((Socio)o)) {
				logger.debug("e' un duplicato");
				errors.reject("duplicato");
			}*/
		}
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return Socio.class.equals(aClass);
	}
}


package it.uniroma3.siw.fym.service;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.fym.model.Attivita;
import it.uniroma3.siw.fym.model.Prenotazione;
import it.uniroma3.siw.fym.model.Socio;
import it.uniroma3.siw.fym.repository.PrenotazioneRepository;

@Service
public class PrenotazioneService {
	@Autowired
	private PrenotazioneRepository prenotazioneRepository;

	@Transactional
	public List<Prenotazione> getAllBySocioId(Long socioId){
		return prenotazioneRepository.findAllBySocioIdOrderByAttivitaDataDesc(socioId);
	}
	
	@Transactional
	public void addPrenotazione(Attivita a, Socio s, String note){
		Prenotazione p = new Prenotazione(a, s, note);
		prenotazioneRepository.save(p);		
	}
	
	@Transactional
	public List<Prenotazione> getAll(){
		return (List<Prenotazione>) prenotazioneRepository.findAll();
	}
	
	@Transactional
	public void deleteById(Long id) {
		prenotazioneRepository.deleteById(id);
	}

	@Transactional
	public Prenotazione getProssimaAttivitaPrenotata(String email) {
		return prenotazioneRepository.findTopBySocioEmailAndAttivitaDataAfterOrderByAttivitaDataAsc(email, Date.from(Instant.now()));
	}
}

package it.uniroma3.siw.fym.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.uniroma3.siw.fym.model.Attivita;
import it.uniroma3.siw.fym.model.Socio;
import it.uniroma3.siw.fym.service.AttivitaService;
import it.uniroma3.siw.fym.service.PrenotazioneService;
import it.uniroma3.siw.fym.service.SocioService;

@Controller
public class PrenotazioneController {

	@Autowired
	private PrenotazioneService prenotazioneService;

	@Autowired
	private AttivitaService attivitaService;
	
	@Autowired
	private SocioService socioService;
	
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    

	@RequestMapping(value = "/prenota", method = RequestMethod.POST)
    public String newPrenotazione(Model model, @RequestParam("attivita") String attivitaId, @RequestParam("note") String note) {
		logger.debug("prenota");
		String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
		Attivita attivita = attivitaService.getById(Long.parseLong(attivitaId));
		prenotazioneService.addPrenotazione(attivita, socioService.getByEmail(email), note);
		attivitaService.riduciPosti(attivita);
		model.addAttribute("prossimaOrganizzata", attivitaService.getProssimaAttivitaOrganizzata(email));
		model.addAttribute("prossimaPrenotata", prenotazioneService.getProssimaAttivitaPrenotata(email));
		return "home.html";
    }

	@RequestMapping(value="/prenotazioni", method = RequestMethod.GET)
    public String getAll(Model model) {
    	logger.debug("getAllPrenotazioni");
    	String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
    	Socio s = socioService.getByEmail(email);
		model.addAttribute("socio", s);
    	model.addAttribute("prenotazioni", prenotazioneService.getAllBySocioId(s.getId()));
        return "prenotazioni.html";
    }
	
	@RequestMapping(value="/eliminaPrenotazione", method = RequestMethod.POST)
    public String deletePrenotazione(Model model, @RequestParam("id") Long id, @RequestParam("aId") Long aId) {
		logger.debug("deletePrenotazione");
		String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
		Socio s = socioService.getByEmail(email);
		model.addAttribute("socio", s);
    	prenotazioneService.deleteById(id);
    	attivitaService.aggiungiPosti(attivitaService.getById(aId));
		model.addAttribute("socio", s);
    	model.addAttribute("prenotazioni", prenotazioneService.getAllBySocioId(s.getId()));
        return "prenotazioni.html";
	}
	
}
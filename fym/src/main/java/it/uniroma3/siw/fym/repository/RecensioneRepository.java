package it.uniroma3.siw.fym.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.fym.model.Recensione;

@Repository
public interface RecensioneRepository extends CrudRepository<Recensione, Long> {
	public List<Recensione> findAllBySocioId(Long id);
	public List<Recensione> findAllBySocioEmailOrderByAttivitaDataDesc(String email);
}

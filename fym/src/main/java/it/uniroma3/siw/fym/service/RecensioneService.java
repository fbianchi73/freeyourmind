package it.uniroma3.siw.fym.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.fym.model.Recensione;
import it.uniroma3.siw.fym.repository.RecensioneRepository;

@Service
public class RecensioneService {
	@Autowired
	private RecensioneRepository recensioneRepository;

	@Transactional
	public List<Recensione> getAllBySocioId(Long socioId){
		return recensioneRepository.findAllBySocioId(socioId);
	}
	
	@Transactional
	public List<Recensione> getAllBySocioEmail(String socioEmail){
		return recensioneRepository.findAllBySocioEmailOrderByAttivitaDataDesc(socioEmail);
	}
	
	@Transactional
	public List<Recensione> getAll(){
		return (List<Recensione>) recensioneRepository.findAll();
	}

	@Transactional
	public void deleteById(Long id) {
		recensioneRepository.deleteById(id);
	}
	
	@Transactional
	public void saveOrUpdate(Recensione recensione) {
		recensioneRepository.save(recensione);
	}
}

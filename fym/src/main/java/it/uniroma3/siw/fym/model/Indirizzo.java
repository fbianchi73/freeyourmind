package it.uniroma3.siw.fym.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Indirizzo {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String indirizzo;

	@Column(nullable=false)
	private String citta;

	@Column(nullable=false)
	private String provincia;

	@Column(nullable=false)
	private String cap;
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(indirizzo);
		str.append(", ");
		str.append(citta);
		str.append(" (");
		str.append(provincia);
		str.append("), ");
		str.append(cap);
		return str.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}
	
	
}

package it.uniroma3.siw.fym.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.fym.model.Credenziali;
import it.uniroma3.siw.fym.model.Indirizzo;
import it.uniroma3.siw.fym.model.Provider;
import it.uniroma3.siw.fym.model.Socio;
import it.uniroma3.siw.fym.service.AttivitaService;
import it.uniroma3.siw.fym.service.CredenzialiService;
import it.uniroma3.siw.fym.service.PrenotazioneService;
import it.uniroma3.siw.fym.validator.SocioValidator;

@Controller
public class MainController {

	@Autowired
	private AttivitaService attivitaService;

	@Autowired
	private CredenzialiService credenzialiService;

	@Autowired
	private PrenotazioneService prenotazioneService;
	
	@Autowired
	private SocioValidator socioValidator;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
	@RequestMapping(value="/private", method = RequestMethod.GET)
    public String getAll(Model model) {
		String email;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal().getClass().equals(User.class))
		{
			UserDetails user = (UserDetails) auth.getPrincipal();
			email = user.getUsername();
		}
		else {
			OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
	        email = oAuth2User.getAttribute("email");
		}
		model.addAttribute("prossimaOrganizzata", attivitaService.getProssimaAttivitaOrganizzata(email));
		model.addAttribute("prossimaPrenotata", prenotazioneService.getProssimaAttivitaPrenotata(email));
        return "home.html";
    }
	
	@RequestMapping(value = "/login")
    public String login(Model model) {
        return "login.html";
    }
	
	@RequestMapping(value="/signup", method = RequestMethod.GET)
    public String addSocio(Model model) {
    	logger.debug("addSocio");
    	model.addAttribute("socio", new Socio());
    	model.addAttribute("credenziali", new Credenziali());
    	model.addAttribute("indirizzo", new Indirizzo());
        return "socioForm.html";
    }
	
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String newSocio(Model model, @ModelAttribute("socio") Socio socio, BindingResult socioBindingResult,
    		@ModelAttribute("credenziali") Credenziali credenziali, BindingResult credenzialiBindingResult) {
		logger.debug("signup");
		credenziali.setUsername(socio.getEmail());
    	this.socioValidator.validate(socio, socioBindingResult);
        if (!socioBindingResult.hasErrors()) {
        	socio.setProvider(Provider.LOCAL);
        	credenziali.setSocio(socio);
			credenzialiService.saveCredenziali(credenziali);
			return "login.html";
        }
        return "socioForm.html";
    }
	
	
}
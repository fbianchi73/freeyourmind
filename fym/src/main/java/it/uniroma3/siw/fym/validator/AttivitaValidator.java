package it.uniroma3.siw.fym.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.fym.model.Attivita;

@Component
public class AttivitaValidator implements Validator{
	private static final Logger logger = LoggerFactory.getLogger(AttivitaValidator.class);

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descrizione", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "data", "required");

		if (!errors.hasErrors()) {
			logger.debug("confermato: valori non nulli");
/*			if (this.socioService.alreadyExists((Socio)o)) {
				logger.debug("e' un duplicato");
				errors.reject("duplicato");
			}*/
		}
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return Attivita.class.equals(aClass);
	}
}


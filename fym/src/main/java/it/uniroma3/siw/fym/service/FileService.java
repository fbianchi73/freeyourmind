package it.uniroma3.siw.fym.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

@Service
public class FileService {
	private static String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/freeyourmind.appspot.com/o/";

	public String upload(MultipartFile multipartFile) {
		try {
			String fileName = multipartFile.getOriginalFilename();                        // to get original file name
			fileName = UUID.randomUUID().toString().concat(this.getExtension(fileName));  // to generated random string values for file name. 
			File file = this.convertToFile(multipartFile, fileName);                      // to convert multipartFile to File
			String TEMP_URL = this.uploadFile(file, fileName);                                   // to get uploaded file link
			file.delete();                                                                // to delete the copy of uploaded file stored in the project folder
			return TEMP_URL;     											                // Your customized response
		} catch (Exception e) {
			e.printStackTrace();
			return "/images/no-img.png";
		}
	}

	private String uploadFile(File file, String fileName) throws IOException {
		BlobId blobId = BlobId.of("freeyourmind.appspot.com", fileName);
		BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
		InputStream is = this.getClass().getClassLoader().getResourceAsStream("key.json");
		Credentials credentials = GoogleCredentials.fromStream(is);
		Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
		storage.create(blobInfo, Files.readAllBytes(file.toPath()));
		String filePath = URLEncoder.encode(fileName, "UTF-8");
		StringBuilder sb = new StringBuilder(DOWNLOAD_URL);
		sb.append(filePath);
		sb.append("?alt=media");
		return sb.toString();
	}

	private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
		File tempFile = new File(fileName);
		try (FileOutputStream fos = new FileOutputStream(tempFile)) {
			fos.write(multipartFile.getBytes());
			fos.close();
		}
		return tempFile;
	}

	private String getExtension(String fileName) {
		return fileName.substring(fileName.lastIndexOf("."));
	}
}

package it.uniroma3.siw.fym.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Socio {
    @Enumerated(EnumType.STRING)
    private Provider provider;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;

	@Column(nullable=false)
	private String cognome;

	private String telefono;

	@Column(nullable=false)
	private String email;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataDiNascita;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Indirizzo indirizzo;
	
	@OneToMany(mappedBy="socio", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Prenotazione> prenotazioni;
	
	@OneToMany(mappedBy="socio", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Recensione> recensioni;

	@OneToMany(mappedBy="organizzatore", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Attivita> attivitaOrganizzate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getDataDiNascita() {
		return dataDiNascita;
	}

	public void setDataDiNascita(LocalDate dataDiNascita) {
		this.dataDiNascita = dataDiNascita;
	}

	public Indirizzo getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(Indirizzo indirizzo) {
		this.indirizzo = indirizzo;
	}

	public List<Prenotazione> getPrenotazioni() {
		return prenotazioni;
	}

	public void setPrenotazioni(List<Prenotazione> prenotazioni) {
		this.prenotazioni = prenotazioni;
	}

	public List<Attivita> getAttivitaOrganizzate() {
		return attivitaOrganizzate;
	}

	public void setAttivitaOrganizzate(List<Attivita> attivitaOrganizzate) {
		this.attivitaOrganizzate = attivitaOrganizzate;
	}
	
	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public List<Recensione> getRecensioni() {
		return recensioni;
	}

	public void setRecensioni(List<Recensione> recensioni) {
		this.recensioni = recensioni;
	}

	public boolean isOrganizzatore(Attivita a) {
		return this.attivitaOrganizzate.contains(a);
	}
	
	public boolean hasPrenotatoAttivita(Attivita a) {
		for(Prenotazione p: this.prenotazioni)
			if(p.getAttivita().equals(a))
				return true;
		return false;
	}
	
	public boolean hasRecensitoAttivita(Attivita a) {
		for(Recensione r: this.recensioni)
			if(r.getAttivita().equals(a))
				return true;
		return false;
	}
}
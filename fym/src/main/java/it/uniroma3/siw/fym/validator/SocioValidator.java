package it.uniroma3.siw.fym.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.fym.model.Socio;
import it.uniroma3.siw.fym.service.SocioService;

@Component
public class SocioValidator implements Validator{
	@Autowired
	private SocioService socioService;
		
    private static final Logger logger = LoggerFactory.getLogger(SocioValidator.class);

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cognome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required");

		Socio s = (Socio) o;
		
		if (!errors.hasErrors()) {
			logger.debug("confermato: valori non nulli");
			if (s.getId() == null && this.socioService.getByEmail(s.getEmail()) != null) {
				logger.debug("duplicato");
				errors.rejectValue("email", "duplicato");
			}
		}
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return Socio.class.equals(aClass);
	}
}

